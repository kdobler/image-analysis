"""
======================================================

                      Homework 2
    ---------------------------------------------

Kalvin Dobler

======================================================
"""


import time
import numpy as np
import matplotlib.pyplot as plt

from scipy import ndimage
from PIL import Image
from skimage.color import rgb2gray
from skimage.transform import resize
from scipy.ndimage import rotate



# ====================================================
#               Exercise 1 - Linear Filtering
# ====================================================

# ----------------------------------------------------
#  1.1
# ----------------------------------------------------

def boxfilter(n):
    """
    A function that returns a filter where values sum up to 1
    :param n: size of the filter
    """
    return np.full(shape = (n,n), fill_value = 1/(n*n))

# ----------------------------------------------------
#  1.2
# ----------------------------------------------------

def convolve_2D(im, filt):
    """
    A function that computes the convolution between an image and a filter
    :param im: a 1D or 2D array
    :param filt: a 1D or 2D array
    :return: an array of the size (m + k - 1) x (n + l - 1)
    """

    # check if the shape[1] is missing
    # if yes, then reshape the array
    try:
        im.shape[1]
    except IndexError:
        im = im.reshape((1, im.shape[0]))

    try:
        filt.shape[1]
    except IndexError:
        filt = filt.reshape((1, filt.shape[0]))

    # flip the kernel, if the kernel is symmetric then it's the same
    filt = np.flip(filt)

    # get the number of pads
    n_pad_h = filt.shape[1] - 1 # the height of the filter - 1
    n_pad_w = filt.shape[0] - 1 # the width of the filter - 1

    # create an empty array of the desired output shape
    convolved_img = np.zeros(shape = (im.shape[0] + n_pad_h, im.shape[1] + n_pad_w))

    # pad the image according to the shape of the inputs
    padded_img = np.pad(im, ((n_pad_h,), (n_pad_w,)), 'constant', constant_values = 0)

    # convolve the image with filter
    # at each location, get the current window defined by the filter shapes
    # convolve the window with the filter and assign the value to the current pixel
    for row in range(0, padded_img.shape[0] - n_pad_h):
        for col in range(0, padded_img.shape[1] - n_pad_w):
            current = padded_img[row:row + filt.shape[1], col:col + filt.shape[0]]
            convolved_img[row, col] = np.sum(np.multiply(current, np.transpose(filt)))

    return convolved_img

# ----------------------------------------------------
#  1.3
# ----------------------------------------------------

img = plt.imread('data/hw2_templates/cat.jpg').astype(np.float32)

box_filter = boxfilter(11)
convolved_img = convolve_2D(img, box_filter)

img.shape
convolved_img.shape

#img.shape
#convolved_img.shape # expected shape: (m+k-1)x(m+l-1)

fig, ax = plt.subplots(1, 2, figsize = (10, 6))
ax[0].imshow(img, cmap = 'gray')
ax[0].set_title('Original image')

ax[1].imshow(convolved_img, cmap = 'gray')
ax[1].set_title('Convolved image')

plt.show()
#plt.savefig('Figure_1_3')

# ----------------------------------------------------
#  1.4
# ----------------------------------------------------

def gauss1D(sigma, filter_length):
    """
    A function that computes a 1D gaussian filter
    :param sigma: standard deviation
    :param filter_length: size of the filter
    :return: a 1D gaussian array
    """
    # if filter length is even, add 1
    if filter_length % 2 == 0:
        filter_length += 1

    # create a mirrored vector of the length given by the filter length
    filt = np.array([i for i in range(-int(np.floor(filter_length/2)), \
                                      int(np.floor(filter_length/2))+1)])

    # each value is compitued from a gaussian and normalized to sum to 1.
    gauss_1d = np.exp(- filt**2 / (2 * sigma**2))
    normalized_filt = gauss_1d / np.sum(gauss_1d)

    return normalized_filt

gauss_1d = gauss1D(1, 3)

# ----------------------------------------------------
#  1.5
# ----------------------------------------------------

def gauss2D(sigma, filter_size):
    """
    A function that computes a 2D gaussian
    :param sigma: standard deviation
    :param filter_size: size of the filter
    :return: a 2D gaussian filter
    """
    gauss_1d = gauss1D(sigma, filter_size)
    gauss_2d = convolve_2D(gauss_1d, np.transpose(gauss_1d))
    return gauss_2d

gauss_2d = gauss2D(3, 11)

plt.imshow(gauss_2d, cmap = 'gray')
plt.show()
#plt.savefig('Figure_1_5')

# ----------------------------------------------------
#  1.6
# ----------------------------------------------------

img = plt.imread('data/hw2_templates/cat.jpg').astype(np.float32)

def convolve_gaussian(img, sigma, filter_size):
    """
    A function that computes the convolution between an image and a 2D gaussian filter
    :param img: image array
    :param sigma: standard deviation
    :param filter_size: size of the filter
    :return: a convolved image
    """
    gauss_2d = gauss2D(3, 11)
    convolved_img = convolve_2D(img, gauss_2d)
    return convolved_img

convolved_gaussian = convolve_gaussian(img, 3, 11)

fig, ax = plt.subplots(1, 2, figsize = (10, 6))
ax[0].imshow(img, cmap = 'gray')
ax[0].set_title('Original image')

ax[1].imshow(convolved_gaussian, cmap = 'gray')
ax[1].set_title('Convolved image with 2D Gaussian')

plt.show()
#plt.savefig('Figure_1_6')

# ----------------------------------------------------
#  1.7
# ----------------------------------------------------

# Convolving an image with a 2D gaussian filter requires more operations that the successive convolution of two 1D filters
# This is the reason why splitting the convolution step in two makes the computation faster
# For that, we could first convolve the image using a 1D filter in the x-direction and subsequently another 1D filter in the y-direction

# ----------------------------------------------------
#  1.8
# ----------------------------------------------------

def compute_convolution_time(img):
    """
    A function that computes the time difference between two 1D filters and one 2D fiter
    :param img: image
    :return: an array of increasing filter size, a 2D array storing the time for each filter type
    """
    filter_sizes = np.arange(3, 100, 5) # vector of different filter sizes
    convolution_time = np.zeros((len(filter_sizes), 2)) # shape = number of filters x number of filter type
    for i in range(len(filter_sizes)):

        gauss_1d = gauss1D(1, filter_sizes[i])
        gauss_2d = gauss2D(1, filter_sizes[i])

        start = time.time()
        conv_1d_x = convolve_2D(img, gauss_1d)
        conv_1d_y = convolve_2D(conv_1d_x, np.transpose(gauss_1d))
        end = time.time() - start

        convolution_time[i, 0] += end

        start = time.time()
        conv_2d = convolve_2D(img, gauss_2d)
        end = time.time() - start

        convolution_time[i, 1] += end

    return filter_sizes, convolution_time

filt_size_ar, conv_time_ar = compute_convolution_time(img)

plt.figure(figsize = (10, 6))
plt.plot(filt_size_ar, conv_time_ar[:, 0], color = 'cornflowerblue')
plt.plot(filt_size_ar, conv_time_ar[:, 1], color = 'mediumorchid')
plt.xlabel('Filter size', fontsize = 14)
plt.ylabel('Time', fontsize = 14)
plt.title('Time-size comparison of two convolution methods', fontsize = 14)
plt.legend(['2x 1D filter', '1x 2D filter'], loc = 'upper left', frameon = False, fontsize = 12)
#plt.savefig('Figure-1_8')


# ====================================================
#               Exercise 2 - Finding Edges
# ====================================================

# ----------------------------------------------------
#  2.1
# ----------------------------------------------------

dx = np.array([-1, 0, 1])
dy = np.transpose(dx)

gauss_1d = gauss1D(1, 3)

sobel_x = convolve_2D(dx, gauss_1d) # equivalent to a sobel x-filter
sobel_y = np.transpose(sobel_x) # equivalen to a sobel y-filter

dx = np.array([1,2,1])
gauss_1d = gauss1D(1,3)
sobel_y_1 = np.transpose(convolve_2D(dx, np.array([1,0,-1])))
sobel_y_1
sobel_y_2 = np.flip(sobel_y_1, axis = 1)
sobel_y_2

test1 = convolve_2D(img, sobel_y_1)
test2 = convolve_2D(img, sobel_y_2)

fig, ax = plt.subplots(1, 2, figsize = (14, 6))

ax[0].imshow(test1, cmap = 'gray')

ax[1].imshow(test2, cmap = 'gray') # remove padding

plt.show()


# ----------------------------------------------------
#  2.2
# ----------------------------------------------------

img = plt.imread('data/hw2_templates/bird.jpg').astype(np.float32)

def compute_image_edge_magn(img, dx, dy):
    """
    A function that constructs an magnitude edge image
    :param img: image
    :param dx: x-derivative filter
    :param dy: y-derivative filter
    :return: magnitude edge image and gradient orientation image
    """
    G_x = convolve_2D(img, dx)
    G_y = convolve_2D(img, dy)

    grad_magnitude = np.sqrt((G_x**2 + G_y**2))

    grad_orientation = np.arctan2(G_y, G_x)

    return  grad_magnitude, grad_orientation

grad_magnitude, grad_orientation = compute_image_edge_magn(img, dx, dy)

fig, ax = plt.subplots(1, 3, figsize = (14, 6))
ax[0].imshow(img, cmap = 'gray')
ax[0].set_title('Original image')

ax[1].imshow(grad_magnitude[1:-1,:], cmap = 'gray') # remove padding
ax[1].set_title('Gradient magnitude')

ax[2].imshow(grad_orientation[1:-1,:], cmap = 'gray') # remove padding
ax[2].set_title('Gradient orientation')

plt.show()
#plt.savefig('Figure-2_2.pdf', dpi=1200)

# ----------------------------------------------------
#  2.3
# ----------------------------------------------------

img = plt.imread('data/hw2_templates/circle.jpg').astype(np.float32)

def compute_edge_maps(img, dx, dy, thrshld):
    """
    A function that returns an 3D array split into 8 equal parts
    :param img: image
    :param dx: x-derivative
    :param dy: y-derivative
    :param thrshld: pixel value threshold
    :return: a 3D array
    """
    # compute the edge map
    grad_magnitude, grad_orientation = compute_image_edge_magn(img, dx, dy)

    # remove the padding to have the same shape as the initial image
    grad_magnitude, grad_orientation = grad_magnitude[1:-1,1:-1], grad_orientation[1:-1,1:-1]

    # set a threshold
    threshold = thrshld

    # create an array of 0s of the same shape as the initial image
    edge_map = np.zeros(shape = (img.shape[0], img.shape[1], 8))

    for iter in range(8):

        # Compute the interval boundaries
        lower_bound, upper_bound = ((2 * np.pi * iter / 8) - np.pi / 8), ((2 * np.pi * iter / 8) + np.pi / 8)

        # Convert the boundaries to the interval [-pi, pi] because np.arctan2 returns [-pi, pi]
        # https://stackoverflow.com/questions/58627711/get-angle-into-range-0-2pi-python
        # https://stackoverflow.com/questions/61479191/convert-any-angle-to-the-interval-pi-pi
        #lower_bound = np.arctan2(np.sin(lower_bound), np.cos(lower_bound))
        #upper_bound = np.arctan2(np.sin(upper_bound), np.cos(upper_bound))

        # Convert the boundaries to the interval [-pi, pi] because np.arctan2 returns value in this [-pi, pi]
        if lower_bound > np.pi:
            lower_bound = lower_bound - 2 * np.pi

        if upper_bound > np.pi:
            upper_bound = upper_bound - 2 * np.pi

        if lower_bound > upper_bound: # happens at the fifth iteration
            edge_map[:, :, iter] = ((grad_magnitude > threshold) & ((lower_bound < grad_orientation) | (upper_bound > grad_orientation)))
        else:
            edge_map[:, :, iter] = ((grad_magnitude > threshold) & (lower_bound < grad_orientation) & (upper_bound > grad_orientation))

    edge_map[edge_map == 1] = 255

    return edge_map

edge_maps = compute_edge_maps(img, sobel_x, sobel_y, 50)

concatenated_edge_maps = edge_maps[:,:,0] + edge_maps[:,:,1] + \
                         edge_maps[:,:,2] + edge_maps[:,:,3] + \
                         edge_maps[:,:,4] + edge_maps[:,:,5] + \
                         edge_maps[:,:,6] + edge_maps[:,:,7]

fig, ax = plt.subplots(3, 3, figsize = (12, 8))

ax[0,0].imshow(concatenated_edge_maps, cmap = 'gray')
ax[0,1].imshow(edge_maps[:,:,0], cmap = 'gray')
ax[0,2].imshow(edge_maps[:,:,1], cmap = 'gray')
ax[1,0].imshow(edge_maps[:,:,2], cmap = 'gray')
ax[1,1].imshow(edge_maps[:,:,3], cmap = 'gray')
ax[1,2].imshow(edge_maps[:,:,4], cmap = 'gray')
ax[2,0].imshow(edge_maps[:,:,5], cmap = 'gray')
ax[2,1].imshow(edge_maps[:,:,6], cmap = 'gray')
ax[2,2].imshow(edge_maps[:,:,7], cmap = 'gray')

plt.tight_layout()
plt.show()
#plt.savefig('Figure-2_3_2', dpi = 1200)

# ----------------------------------------------------
#  2.4
# ----------------------------------------------------

img = plt.imread('data/hw2_templates/bird.jpg').astype(np.float32)

def compute_non_max_suppression(grad_magnitude, edge_maps):
    """
    A function that compute non-maximum suppression
    :param grad_magnitude: magnitude edge image
    :param edge_maps: edge maps
    :return: edge maps with thinner edges
    """

    # pad the magnitude edge image so that we don't have troubles when selecting neighbor pixels at the boundaries
    padded_grad_magnitude = np.pad(grad_magnitude, (1, 1), 'constant', constant_values = 0)

    # loop through each dimension and pixel of the edge maps
    # select the neighbor pixels according to the respective edge direction
    # if a pixel has an intensity value of 255, then select the neighbor pixels
    # if the pixel has a lower intensity value than one of its neighbors, then set it to 0

    for i in range(8): # edge direction from 0 to 7
        for row in range(0, edge_maps.shape[0] - 1):
            for col in range(0, edge_maps.shape[1] - 1):

                # if edge direction = top or bottom, and pixel value = 255
                if (i == 0 | i == 4) & (edge_maps[row, col, i] == 255):
                    top, bottom = grad_magnitude[row-1, col], grad_magnitude[row+1, col]
                    if (grad_magnitude[row, col] < (top or bottom)):
                        edge_maps[row, col, i] = 0

                # if edge direction = left or right, and pixel value = 255
                elif (i == 2 | i == 6) & (edge_maps[row, col, i] == 255):
                    left, right = grad_magnitude[row, col-1], grad_magnitude[row, col+1]
                    if (grad_magnitude[row, col] < (left or right)):
                        edge_maps[row, col, i] = 0

                # if edge direction = north west or south east, and pixel value = 255
                elif (i == 1 | i == 5) & (edge_maps[row, col, i] == 255):
                    topleft, bottomright = grad_magnitude[row-1, col-1], grad_magnitude[row+1, col+1]
                    if (grad_magnitude[row, col] < (topleft or bottomright)):
                        edge_maps[row, col, i] = 0

                # if edge direction = north east or south west, and pixel value = 255
                elif (i == 3 | i == 7) & (edge_maps[row, col, i] == 255):
                    topright, bottomleft = grad_magnitude[row-1, col+1], grad_magnitude[row+1, col-1]
                    if (grad_magnitude[row, col] < (topright or bottomleft)):
                        edge_maps[row, col, i] = 0

    return edge_maps

grad_magnitude, grad_orientation = compute_image_edge_magn(img, sobel_x, sobel_y)

edge_maps = compute_edge_maps(img, sobel_x, sobel_y, 80)

non_max_suppression_maps = compute_non_max_suppression(grad_magnitude[1:-1, 1:-1], edge_maps)

concatenated_non_max = non_max_suppression_maps[:,:,0] + non_max_suppression_maps[:,:,1] + \
                       non_max_suppression_maps[:,:,2] + non_max_suppression_maps[:,:,3] + \
                       non_max_suppression_maps[:,:,4] + non_max_suppression_maps[:,:,5] + \
                       non_max_suppression_maps[:,:,6] + non_max_suppression_maps[:,:,7]

fig, ax = plt.subplots(1, 3, figsize = (14, 6))

ax[0].imshow(img, cmap = 'gray')
ax[0].set_title('Original image')

ax[1].imshow(grad_magnitude[1:-1,1:-1], cmap = 'gray')
ax[1].set_title('Magnitude edge image')

ax[2].imshow(concatenated_non_max, cmap = 'gray')
ax[2].set_title('Non-max suppression image')

plt.tight_layout()
plt.show()
#plt.savefig('Figure-2_4', dpi = 1200)


# ====================================================
#               Exercise 3 - Corner Detection
# ====================================================

# ----------------------------------------------------
#  3.1
# ----------------------------------------------------

img = plt.imread('data/hw2_templates/chessboard.jpeg').astype(np.float32)

def compute_harris_corner(img, w_size, sigma, k, filter_size = 10):
    """
    A function that computes the harris corner at each pixel
    :param img: 2D image
    :param w_size: window size
    :param sigma: standard deviation
    :param k: harris corner parameter
    :param filter_size: filter size
    :return: harris response array
    """

    # convert the image to grayscale
    img = rgb2gray(img)

    # apply gaussian blurring
    #img_blurred = ndimage.gaussian_filter(img_grayscale, sigma = 1)

    # create the derivative filters
    dx = np.array([-1, 0, 1])
    gauss_1d = gauss1D(sigma, filter_size)

    # create the sobel filters
    sobel_x = convolve_2D(dx, gauss_1d)
    sobel_y = np.transpose(sobel_x)

    # compute the gradient of the image in both directions
    I_x = convolve_2D(img, sobel_x)
    I_y = convolve_2D(img, sobel_y)

    # remove the padding so that the derivative images have the same shape, which is required for upcoming operations
    n_pad_h_x = int((sobel_x.shape[1] - 1) / 2)
    n_pad_w_x = int((sobel_x.shape[0] - 1) / 2)
    I_x = I_x[n_pad_h_x:-n_pad_h_x, n_pad_w_x:-n_pad_w_x]

    n_pad_h_y = int((sobel_y.shape[1] - 1) / 2)
    n_pad_w_y = int((sobel_y.shape[0] - 1) / 2)
    I_y = I_y[n_pad_h_y:-n_pad_h_y, n_pad_w_y:-n_pad_w_y]

    # compute the product of derivatives at each pixel
    I_xx = I_x **2
    I_xy = I_x * I_y
    I_yy = I_y **2

    # compute sums of products of derivatives at each pixel over a window of size w_size of ones
    w_filt = np.ones(shape = (w_size, w_size))
    S_xx = convolve_2D(I_xx, w_filt)
    S_xy = convolve_2D(I_xy, w_filt)
    S_yy = convolve_2D(I_yy, w_filt)

    # compute the determinant of the covariance matrix
    detH = S_xx * S_yy - S_xy**2

    # compute the trace of the covariance matrix
    traceH = S_xx + S_yy

    # compute the response of the detector
    harris_response = detH - (k * traceH**2)

    # the next procedure is to select R values ... is it useful?
    # create an array of 0s of the same size of the original image
    #img_corner = np.zeros_like(img)

    # define the threshold for corner
    #threshold = 10000

    # create a mask for value > threshold, defined as corner
    #corner_mask = harris_response[2:-2, 2:-2] > threshold
    #img_corner[corner_mask] = 255

    return harris_response#, img_corner

hr = compute_harris_corner(img, 5, 2, 0.1)

# ----------------------------------------------------
#  3.2
# ----------------------------------------------------

fig, ax = plt.subplots(1, 2, figsize = (10, 6))
ax[0].imshow(img.astype('uint8'), cmap = 'gray')
ax[0].set_title('Original image')

ax[1].imshow(hr, cmap = 'gray')
ax[1].set_title('Corner detection')

plt.show()
#plt.savefig('Figure-3_2', dpi = 1200)

# ----------------------------------------------------
#  3.3
# ----------------------------------------------------

rotated_img = rotate(img, angle = 45)

rotated_hr = compute_harris_corner(rotated_img, 5, 0.2, 0.1)

fig, ax = plt.subplots(1, 2, figsize = (10, 6))
ax[0].imshow(img.astype('uint8'), cmap = 'gray')
ax[0].set_title('Original image')

ax[1].imshow(rotated_hr, cmap = 'gray')
ax[1].set_title('Harris corner rotated image')

plt.show()
#plt.savefig('Figure-3_3_r')

# ----------------------------------------------------
#  3.4
# ----------------------------------------------------

downscaled_img = resize(img, (int(img.shape[0]/2), int(img.shape[1]/2)))

downscaled_hr = compute_harris_corner(downscaled_img, 5, 0.2, 0.1)

fig, ax = plt.subplots(1, 2, figsize = (10, 6))
ax[0].imshow(img.astype('uint8'), cmap = 'gray')
ax[0].set_title('Original image')

ax[1].imshow(downscaled_hr, cmap = 'gray')
ax[1].set_title('Harris corner downscaled image')

plt.show()
#plt.savefig('Figure-3_4_d')

# ----------------------------------------------------
#  3.5
# ----------------------------------------------------

# Harris corner method is rotation invariant, but not scale invariant.
# Changing the rotation rotates the eigenvectors but leaves the eigenvalues unchanged, leading to the rotation of the ellpise.
# Changing the scale of the image affects the eigenvalues, thus leading to a different ellipse scale.